from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)


@app.route('/book/')
def showBook():
    return render_template('showBook.html',books = books)

@app.route('/book/new1/', methods=['POST'])
def new():
    new_book = request.form["name"]
    next_id = int(books[-1]["id"]) + 1
    books.append({'title': new_book, "id": str(next_id)})
    return render_template('showBook.html', books=books)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/process/', methods=['POST'])
def process(book_id):
    new_name= request.form['name']
    for i in books:
        if i["id"] == str(book_id):
            i["title"] = new_name      
    return render_template('showBook.html', books=books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    return render_template('editBook.html', book_id=book_id)
	
@app.route('/book/<int:book_id>/delete1/', methods=['POST'])
def delete(book_id):
    index = 0
    for i in books:
        if i["id"] == str(book_id):
            books.pop(index)
        index +=1
    return render_template('showBook.html', books=books)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	return render_template('deleteBook.html', book_id=book_id, books = books)
	
    

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

